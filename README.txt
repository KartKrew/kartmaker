Kartmaker, maintained by Kart Krew Dev

DR. ROBOTNIK'S RING RACERS KART SPRITES BY VELOCITONI, PERMISSIONS:
- SRB2 Message Board: only for use within the context of Ring Racers
  modifications; as long as it's for Ring Racers it can be modified or edited
  and used without permission.
- Just ask for permission from VelocitOni for any use otherwise.

HOW TO USE:
1. Create a copy of the example-input directory and rename it to your preferred
   output filename.
2. Select a template spritesheet and corresponding properties.txt file from a
   directory in the sprite-templates directory and copy them (overwriting the
   same-named files) into the directory made in step 1.
3. Edit the top of "properties.txt" to set various kart stats for your racer.
   The rest of the file defines parameters for the template, so leave those
   alone unless you’ve changed the template background, positions, etc.
4. Create or paste your racer's sprites into the kart template (if you're
   pasting, make sure to erase the wheel shine from the sprites so that the
   converter can apply the proper shine for each frame!)
5. Replace the sound files in the folder with OGG sounds that you want your
   racer to have (this program does not convert or add echo for you)
6. Drag the folder onto the kartmaker executable, or run it and pass the folder
   as an argument.
7. The output file will be placed next to your character's source folder, ready
   to play!

Ignore build.bat/sh and the src directory for standard use. This is a compile
script and source files. You can use it if you want to modify the program (you
need GCC installed). I make no guarantees that the program compiles on other
setups.

Licensed under the GPL.
